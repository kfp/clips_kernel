#!/usr/bin/env python
# -*- coding: UTF-8 -*-


__author__ = "Kurt Pagani <nilqed@gmail.com>"
__svn_id__ = "$Id: theKernel.py 1 2015-09-28 19:28:05Z pagani $"


from ipykernel.kernelbase import Kernel
import re, os, tempfile

if os.name == 'nt':
    import winpexpect as xp
    spawn = xp.winspawn
else:
    import pexpect as xp
    spawn = xp.spawn


__version__ = '0.1'

#====================#
# User configuration #
#====================#

# theApp
executable = "clips"
prompt_pat = "CLIPS>"
cont_char = ""
quit_cmd = "(exit)"
readq = ''
tmpfile_kw = {'prefix':'ax$', 'suffix':'.input', 'delete':False}
type_color = r"blue"
type_size = r"\scriptsize"
tex_color = r"black"
tex_size = r"\normalsize"


# theKernel
_protocol_version_ = '5.0'
_implementation_ = 'clips_kernel'
_language_ = 'clips'
_language_version_ = '0.1'
_language_info_ = {'name': 'clips', 'mimetype': 'text/plain'}
_banner_ = "CLIPS Kernel"
_help_links_ = {'text': 'CLIPS HELP', 'url': ''}



#==============#
# theApp class #
#==============#

class theApp():

    def __init__(self, app = executable, re_prompt = prompt_pat ):

        # Store parameters
        self.app = app
        self.re_prompt = re_prompt

        # Define the App process
        self.axp = None

        # The header/banner read when started
        self.banner = None

        # The current prompt
        self.prompt = None

        # Output caught after a command has been sent (always unmodified)
        self.output = None

        # Last error enountered (0: OK, 1: EOF, 2: TIMEOUT)
        self.error = None

        # Log file ([win]spawn)
        self.logfile = None


    def _axp_expect(self):
        """
        Return True if the prompt was matched otherwise return False and
        set the error = 1 if EOF or error = 2 if TIMEOUT.
        """
        self.error = self.axp.expect([self.re_prompt, xp.EOF, xp.TIMEOUT])
        if self.error == 0:
            self.error = None
            return True
        else:
            return False


    def _axp_sendline(self, txt):
        """
        Send the text + os.linesep to the App and expect the prompt. Moreover
        reset the error state. Return value is as in _axp_expect.
        """
        self.error = None
        n = self.axp.sendline(txt)
        return self._axp_expect()


    def start(self, **kwargs):
        """
        Action: Start (spawning) theApp.
        Return: True or False
        The following keywords (kwargs) may be used:
        args=[], timeout=30, maxread=2000, searchwindowsize=None
        logfile=None, cwd=None, env=None, username=None, domain=None
        password=None
        For details consult the pexpect manual as this parameters are
        the same as in the spawn/winspawn function respectively.
        Note: after start one may access the values as follows:
        <app_instance>.axp.<keyword>, e.g. a.axp.timeout -> 30.
        """
        if self.axp is None:
            self.axp = spawn(self.app, **kwargs)
            if os.name != "nt" : self.axp.setecho(False)
            #error on NT

        if self._axp_expect():
            self.banner = self.axp.before
            self.prompt = self.axp.after
            return True

        else:
            return False


    def stop(self):
        """
        Stop theApp (the hard way). One may also send the native quit command.
        to the App using writeln for example.
        The return value is that of the isalive() function.
        """
        if self.axp is not None:
            self.axp.close()
            self.axp = None
        return not self.isalive()


    def isalive(self):
        """
        Check if theApp is running.
        """
        if self.axp is not None:
            return self.axp.isalive()
        else:
            return False


    def haserror(self):
        """
        True if there was an error.
        """
        return self.error is not None


    def hasoutput(self):
        """
        True if there is output.
        """
        return self.output is not None


    def writeln(self, src):
        """
        Write a line to theApp, i.e. as if it were entered into the
        interactive console. Output - if any - is (unmodified) stored
        in 'output'. Note: src should not contain any control characters;
        a newline (in fact os.linesep) will be added automatically.
        The continuation character, however, is no problem.
        """
        if self._axp_sendline(src):
            self.output = self.axp.before
            self.prompt = self.axp.after
            return True
        else:
            self.output = None
            return False


    def writef(self, filename):
        """
        Write the content of the file to theApp, i.e. urge theApp to read it
        in by itself.
        """
        if os.path.isfile(filename):
            return self.writeln(readq.format(filename))
        else:
            return False


    def write(self, src): # not needed here
        """
        Place the string src into a temp file and call writef, that
        is command theApp to read in the temp file. Note: the temp file
        will be deleted after having been read into theApp.
        This command allows multiline input in SPAD/Aldor form.
        """
        tmpf = tempfile.NamedTemporaryFile(**tmpfile_kw)
        tmpf.write(src)
        tmpf.close()
        rc = self.writef(tmpf.name)
        os.unlink(tmpf.name)
        return rc


#=================#
# theKernel class #
#=================#

class theKernel(Kernel):

    implementation = _implementation_
    implementation_version = __version__
    language = _language_
    language_version = _language_version_
    language_info = _language_info_
    banner = _banner_
    # help_links = _help_links_
    # protocol_version = _protocol_version_


    def __init__(self, **kwargs):
        Kernel.__init__(self, **kwargs)
        self.app = theApp()
        self.app.start()


    def do_execute(self, code, silent, store_history = True,
        user_expressions = None, allow_stdin = False):

        if not code.strip():
            return {'status': 'ok', 'execution_count': self.execution_count,
                    'payload': [], 'user_expressions': {}}


        try:
            if code.strip() == quit_cmd:
                return self.do_shutdown(False)

            # send code to app
            self.app.writeln(code.rstrip())


            if self.app.hasoutput:
              output = self.app.output
            else:
              output = None


            data = {'text/plain':output}
            output = None


            if not silent:
                display_data = {'source':'pax', 'data':data, 'metadata':{}}
                self.send_response(self.iopub_socket, 'display_data', display_data)

            if interrupted:
                return {'status': 'abort', 'execution_count': self.execution_count}

        except:
            self.app.output = None


        try:
            exitcode = 0
            if not self.app.isalive():
                exitcode = 1

        except Exception:
            exitcode = 1


        if exitcode:
            return {'status': 'error', 'execution_count': self.execution_count,
                    'ename': '', 'evalue': str(exitcode), 'traceback': []}
        else:
            return {'status': 'ok', 'execution_count': self.execution_count,
                    'payload': [], 'user_expressions': {}}


    def do_complete(self, code, cursor_pos):
        pass


    def do_inspect(self, code, cursor_pos, detail_level=0):
        pass


    def do_shutdown(self, restart):
        "Changes in 5.0: <data> replaced by <text>"
        output = "-- Bye. Kernel shutdown "
        stream_content = {'name': 'stdout', 'text': output}
        self.send_response(self.iopub_socket, 'stream', stream_content)
        self.app.writeln(quit_cmd)
        self.app.stop()
        return {'restart': False}




if __name__ == '__main__':

    from ipykernel.kernelapp import IPKernelApp
    IPKernelApp.launch_instance(kernel_class=theKernel)

