
# CLIPS backend for Jupyter

[CLIPS](http://clipsrules.sourceforge.net/) is a productive development and delivery expert system tool which provides a complete environment for the construction of rule and/or object based expert systems.

### Installation

Get this repository: 

    git clone https://bitbucket.org/kfp/clips_kernel.git
  
Change to `clips_kernel` and run 
    
    python kernel_setup.py install --user
    
  
For a global installation ommit the option `--user`.

### Notebook

Start the notebook:

    ipython notebook 
    
Point your browser to `http://localhost:8888` and choose the kernel by 
the menu `New -> CLIPS`. 




```clips

```
