from distutils.core import setup
from distutils.command.install import install
import json
import os.path
import sys

kernel_json = {"argv":[sys.executable, "-m", "clips_kernel", "-f",
               "{connection_file}"], "display_name":"CLIPS",
               "language":"clips", "codemirror_mode":"shell"
}

class install_with_kernelspec(install):

    def run(self):

        install.run(self)

        # Kernel specification
        from ipykernel.kernelspec import KernelSpecManager
        from IPython.utils.path import ensure_dir_exists
        destdir = os.path.join(KernelSpecManager().user_kernel_dir, 'clips')
        ensure_dir_exists(destdir)
        with open(os.path.join(destdir, 'kernel.json'), 'w') as f:
            json.dump(kernel_json, f, sort_keys=True)

svem_flag = '--single-version-externally-managed'
if svem_flag in sys.argv:
    sys.argv.remove(svem_flag)

setup(name='clips_kernel',
      version='0.1',
      description='A CLIPS kernel for Jupyter',
      long_description="CLIPS wrapper kernel for Jupyter",
      author='Kurt Pagani',
      author_email='nilqed@gmail.com',
      url='https://bitbucket.com/kfp/clips_kernel',
      py_modules=['clips_kernel'],
      cmdclass={'install': install_with_kernelspec},
      classifiers = [
          'Framework :: IPython',
          'License :: OSI Approved :: BSD License',
          'Programming Language :: Python :: 2',
          'Topic :: KB :: CLIPS',
      ]
)